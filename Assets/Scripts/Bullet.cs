﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Projectile
{
    public float lifeTime = 5;
    float counter = 0;

    void Update()
    {
        rb.velocity = transform.TransformDirection(new Vector2(1, 0) * speed);
        
        if (counter >= lifeTime)
        {
            Destroy(gameObject);
        }
        else
        {
            counter += 0.1f;
        }
    }
}
