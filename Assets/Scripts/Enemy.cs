﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    Transform player;
    private Rigidbody2D rb;

    //movement variables
    [SerializeField]
    private float speed = 10;
    [SerializeField]
    private  float stoppingDistance = 0;
    [SerializeField]
    private  float retreatDistance= 0;

    //fire variables
    [SerializeField]
    private bool doesFire = true;
    public GameObject bullet;
    [SerializeField]
    private  int damage = 1;
    [SerializeField]
    private  float fireDistance = 0;
    [SerializeField]
    private  float fireDelay = 0;
    private float nextFire = 0.0f;



    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        //movement
        //move towards player
        if(Vector2.Distance(transform.position, player.position) > stoppingDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        }
        //stop at a distance from player
        else if (Vector2.Distance(transform.position, player.position) < stoppingDistance && Vector2.Distance(transform.position, player.position) > retreatDistance)
        {
            transform.position = this.transform.position;
        }
        //retreat at a distance from player
        else if (Vector2.Distance(transform.position, player.position) < retreatDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
        }

        if (doesFire) { Fire(); }

    }

    void Fire()
    {

        if (Vector2.Distance(transform.position, player.position) < fireDistance && Time.time > nextFire)
        {
            nextFire = Time.time + fireDelay;
            GameObject bulletInst = Instantiate(bullet, transform.position, Quaternion.identity);
            bulletInst.layer = 9;
            bulletInst.GetComponent<EnemyBullet>().damage = damage;
        }
    }
}
