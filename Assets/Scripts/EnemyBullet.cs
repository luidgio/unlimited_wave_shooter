﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : Projectile
{
    private GameObject player;
    private Vector2 target;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform.position;
    }

    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
        if (new Vector2(transform.position.x, transform.position.y) == target)
        {
            Destroy(gameObject);
        }
    }
}
