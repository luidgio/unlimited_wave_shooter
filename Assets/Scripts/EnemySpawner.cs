﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject [] enemy;
    public GameObject [] spawnArea;

    public void SpawnEnemies(int numberOfEnemies, float delay)
    {
        //StopCoroutine(SpawnEnemiesCoroutine(numberOfEnemies, delay));
        StartCoroutine(SpawnEnemiesCoroutine(numberOfEnemies, delay));
    }

    private void SpawnEnemyInArea (int enemyIndex, int areaIndex)
    {
        Vector2 areaMin = spawnArea[areaIndex].GetComponent<BoxCollider2D>().bounds.min;
        Vector2 areaMax = spawnArea[areaIndex].GetComponent<BoxCollider2D>().bounds.max;

        Instantiate(enemy[enemyIndex], new Vector3(Random.Range(areaMin[0], areaMax[0]), Random.Range(areaMin[1], areaMax[1]), 0.0f), Quaternion.AngleAxis(0, Vector3.forward));
    }

    private IEnumerator SpawnEnemiesCoroutine (int numberOfEnemies, float delay)
    {
        for (int i = 0; i < numberOfEnemies; i++)
        {
            SpawnEnemyInArea(Random.Range(0, enemy.Length), Random.Range(0, spawnArea.Length));
            yield return new WaitForSeconds(delay);
        }
    }
}
