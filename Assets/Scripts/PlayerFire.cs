﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour
{
    public GameObject bullet; //The bullet object
    public float fireDelay = 0.5f;
    float nextFire = 0.0f;
    int shootingDir;

    void Update()
    {
        CheckFireKey("FireH");
        CheckFireKey("FireV");
    }

    void CheckFireKey(string key)
    {
        if (Input.GetButton(key) && Time.time > nextFire)
        {
            nextFire = Time.time + fireDelay;
            if (Input.GetAxisRaw(key) > 0)
            {
                shootingDir = key == "FireH" ? 0 : 90; // check if input horizontal or vertical
            }
            else
            {
                shootingDir = key == "FireH" ? 180 : 270; // check if input horizontal or vertical
            }
            Fire();
        }
    }

    void Fire()
    {
        GameObject bulletInst = Instantiate(bullet, transform.position, Quaternion.AngleAxis(shootingDir, Vector3.forward));
        bulletInst.layer = 8;
    }
}
