﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [HideInInspector]
    public int damage = 1;
    public float speed = 10;
    protected Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        ITakeDamage itd = other.GetComponent<ITakeDamage>();
        if (itd != null)
        {
            other.attachedRigidbody.AddForce(rb.velocity * 100);
            itd.ITakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
