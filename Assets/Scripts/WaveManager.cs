﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    private int wave;
    private int difficulty;

    EnemySpawner enemySpawner;

    void Awake()
    {
        enemySpawner = GetComponent<EnemySpawner>();
    }

    public void NextWave()
    {
        enemySpawner.SpawnEnemies(3 + difficulty, 0.5f);
        wave += 1;
        difficulty = wave / 10;
    }

}
